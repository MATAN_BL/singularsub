import re
import xlrd
import datetime
import config


def cell_value_to_date(workbook, cell_value):
    dt_tuple = xlrd.xldate_as_tuple(cell_value, workbook.datemode)
    get_col = datetime.datetime(dt_tuple[0], dt_tuple[1], dt_tuple[2])
    return "{}-{}-{}".format(get_col.year, get_col.month, get_col.day)


def str_to_float(str_num):
    return float(re.sub('[^(0-9)|.]', '', str_num))


def get_row(worksheet, row_no):
    row = [worksheet.cell(row_no, i).value for i in range(0, worksheet.ncols)]
    return row


def get_last_row_in_table(worksheet):
    row_no = config.row_number_of_title + 1
    while True:
        if is_row_in_table(worksheet, row_no) == False:
            return row_no
        row_no += 1


def get_row_number_of_title(worksheet):
    for row_no in range(0, worksheet.nrows):
        row = get_row(worksheet, row_no)
        if row == ["Date", "App", "Campaign", "Country", "Cost", "Installs"]:
            return row_no


def is_row_in_table(worksheet, row_no):
    row = get_row(worksheet, row_no)
    if filter(lambda x: str(x) == "", row) == []:
        return True
    else:
        return False