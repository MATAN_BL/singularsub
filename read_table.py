from accessory_functions import *
import config
import sys


def get_total_installs_and_cost_per_app_and_platford(worksheet, workbook):
    installs_cost_per_app_and_platform = {}
    for row_no in range(config.row_number_of_title + 1, config.last_row_in_table):
        app = str(worksheet.cell(row_no, 1).value)
        if app not in installs_cost_per_app_and_platform.keys():
            installs_cost_per_app_and_platform[app] = {'iOS': {'cost': 0, 'installs': 0}, 'Android': {'cost': 0, 'installs': 0}}
        if "iOS" in str(worksheet.cell(row_no, 2).value):
            platform = 'iOS'
        else:
            platform = 'Android'
        installs_cost_per_app_and_platform[app][platform]['cost'] += str_to_float(str(worksheet.cell(row_no, 4).value))
        installs_cost_per_app_and_platform[app][platform]['installs'] += str_to_float(str(worksheet.cell(row_no, 5).value))
    return installs_cost_per_app_and_platform


def get_total_installs_and_cost_per_date(worksheet, workbook):
    installs_cost_per_day = {}
    for row_no in range(config.row_number_of_title + 1, config.last_row_in_table):
        date_str = cell_value_to_date(workbook, worksheet.cell(row_no, 0).value)
        if date_str not in installs_cost_per_day.keys():
            installs_cost_per_day[date_str] = {'cost': 0, 'installs': 0}
        installs_cost_per_day[date_str]['cost'] += str_to_float(str(worksheet.cell(row_no, 4).value))
        installs_cost_per_day[date_str]['installs'] += str_to_float(str(worksheet.cell(row_no, 5).value))
    return installs_cost_per_day


def get_total_col(worksheet, col_num):
    sum = 0.0
    for row_no in range(config.row_number_of_title + 1, config.last_row_in_table):
        sum += str_to_float(str(worksheet.cell(row_no, col_num).value))
    return sum


def main():
    filename = sys.argv[1]
    try:
        workbook = xlrd.open_workbook(filename)
    except:
        print "Not a legal xsl file"
        sys.exit(1)

    worksheet = workbook.sheet_by_index(0)
    config.row_number_of_title = get_row_number_of_title(worksheet)
    config.last_row_in_table = get_last_row_in_table(worksheet)
    print "Total Cost: " + '${}'.format(get_total_col(worksheet, 4)) # get total costs
    print "Total Installs: " + '{}'.format(int(get_total_col(worksheet, 5)))
    print "Total Installs and Total Cost per Day: ", get_total_installs_and_cost_per_date(worksheet, workbook)
    print "Total Installs and Total Cost per each App and Platform: ", get_total_installs_and_cost_per_app_and_platford(worksheet, workbook)

if __name__ == '__main__':
    main()
