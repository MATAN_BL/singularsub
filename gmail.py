import imaplib, email, os, sys

try:
    mail = imaplib.IMAP4_SSL('imap.gmail.com', 993)
    mail.login(sys.argv[1], sys.argv[2])
    mail.list()
    mail.select("inbox")
    svdir = sys.argv[3]
except:
    print "please activate the file by typing: 'python gmail.py <email_address> <password> <path_to_directory>"
    sys.exit(1)


def download_file(data_id):
    result, email_data = mail.uid('fetch', data_id, '(RFC822)')
    raw_email = email_data[0][1]
    raw_email_string = raw_email.decode('utf-8')
    m = email.message_from_string(raw_email_string)

    if m.get_content_maintype() != 'multipart':
        os.exit(1)

    for part in m.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue

        filename = part.get_filename()
        if filename is not None and ".xlsx" in filename:
            sv_path = os.path.join(svdir, filename)
            if not os.path.isfile(sv_path):
                print sv_path
                fp = open(sv_path, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()


result, data = mail.uid('search', None, '(HEADER Subject "Singular Python Exercise")')
map(download_file, data[0].split())



